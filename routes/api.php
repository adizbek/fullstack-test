<?php

use App\Http\Controllers\Api\ArticleController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('articles')->name('articles.')->group(function () {
    Route::get('/', [ArticleController::class, 'listArticles'])->name('list');
    Route::get('/article/{slug}', [ArticleController::class, 'findBySlug'])->name('find-by-slug');

    Route::post('importFeed', [ArticleController::class, 'importFeed'])->name('import-feed');
    Route::get('categories', [ArticleController::class, 'getCategories'])->name('categories');
});
