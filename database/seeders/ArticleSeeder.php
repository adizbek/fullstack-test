<?php

namespace Database\Seeders;

use App\Domain\Feed\FeedLoader\JSONFileLoader;
use App\Domain\Feed\FeedService;
use Illuminate\Database\Seeder;

class ArticleSeeder extends Seeder
{
    public function run()
    {
        $loader = new JSONFileLoader(base_path('tests/Services/FeedLoader/test_feed.json'));
        $articles = $loader->load();

        $repository = app()->make(FeedService::class);
        $repository->saveFeeds($articles);
    }
}
