<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->uuid('feed_id')->nullable();
            $table->string('source', 50)->nullable();
            $table->string('title', 200)->nullable();
            $table->string('subtitle', 200)->nullable();
            $table->string('format', 20)->nullable();
            $table->string('emoji', 10)->nullable();
            $table
                ->string('slug')
                ->unique()
                ->nullable();
            $table->text('content')->nullable();

            $table->timestamps();
        });

        Schema::create('categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 50);
        });

        Schema::create('article_categories', function (Blueprint $table) {
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('article_id');
            $table->primary(['category_id', 'article_id']);
            $table->boolean('is_primary');
        });

        Schema::create('media', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('article_id')->index();

            $table->uuid('media_id');
            $table->string('source', 50)->nullable();
            $table->string('slug')->unique();
            $table->string('type', 20);
            $table->json('attributes')->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('articles');
        Schema::dropIfExists('categories');
        Schema::dropIfExists('article_categories');
        Schema::dropIfExists('media');
    }
}
