<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::table('media', function (Blueprint $table) {
            $table->dropColumn(['article_id']);
        });

        Schema::create('article_media', function (Blueprint $table) {
           $table->unsignedBigInteger('article_id');
           $table->unsignedBigInteger('media_id');

           $table->primary(['article_id', 'media_id']);
        });
    }

    public function down()
    {
        Schema::table('media', function (Blueprint $table) {
            $table->unsignedBigInteger('article_id');
        });

        Schema::dropIfExists('article_media');
    }
};
