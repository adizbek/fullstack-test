<?php

namespace Tests\Services\FeedLoader;

use App\Domain\Feed\FeedLoader\JSONFileLoader;
use Tests\TestCase;

class JSONFileLoaderTest extends TestCase
{
    public function testLoad()
    {
        $loader = new JSONFileLoader(base_path('tests/Services/FeedLoader/test_feed.json'));

        $articles = $loader->load();

        $this->assertCount(2, $articles);

        $article = $articles[0];

        $this->assertStringContainsString(
            'Cosmology in crisis as evidence suggests our universe',
            $article->getTitle(),
        );
        $this->assertStringContainsString('Make photography fun again', $article->getSubtitle());
        $this->assertStringContainsString('cosmology-in-crisis-as-evidence-suggests', $article->getSlug());
        $this->assertStringContainsString('102caa00-fe4c-5f6a-bdc0-8650e3ba1960', $article->getFeedId());
        $this->assertEquals('📄', $article->getEmoji());
        $this->assertEquals('archive', $article->getSource());
        $this->assertEquals('standard', $article->getFormat());
        $this->assertStringContainsString(
            'No matter how elegant your theory is, experimental data will ',
            $article->getContent(),
        );
        $this->assertContainsAll(['syndication', 'tech', 'space'], $article->getCategories());
        $this->assertCount(1, $article->getMedia());

        $media = $article->getMedia()[0];

        $this->assertEquals('featured', $media->getType());
        $this->assertEquals(
            'copy-of-copy-of-copy-of-copy-of-copy-of-copy-of-2-3',
            $media->getSlug(),
        );
        $this->assertEquals('archive', $media->getSource());
        $this->assertEquals('85b597bf-7472-5b72-99c8-d304f5d3a3e5', $media->getMediaId());
        $this->assertEquals(1200, $media->getAttributes()->width);
    }
}
