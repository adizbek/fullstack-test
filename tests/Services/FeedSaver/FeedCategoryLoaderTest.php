<?php

namespace Tests\Services\FeedSaver;

use App\Domain\Category\FeedCategoryLoader;
use App\Domain\Feed\FeedLoader\JSONFileLoader;
use Tests\TestCase;

class FeedCategoryLoaderTest extends TestCase
{
    public function testLoad()
    {
        $loader = new JSONFileLoader(base_path('tests/Services/FeedLoader/test_feed.json'));

        $articles = $loader->load();

        $loader = new FeedCategoryLoader($articles);

        $this->assertContainsAll(['syndication', 'tech', 'space', 'plugged'], $loader->load());
    }
}
