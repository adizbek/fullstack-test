<?php

namespace Tests\Http\Controllers\Api;

use App\Models\Category;
use Database\Seeders\ArticleSeeder;
use Tests\TestCase;

class ArticleControllerTest extends TestCase
{
    public function testImportFeed()
    {
        $response = $this->postJson(route('articles.import-feed'), [
            'file' => $this->uploadTestFile(
                base_path('tests/Services/FeedLoader/test_feed.json'),
            ),
        ]);

        $response->assertSuccessful();
        $response->assertJson(['imported' => true]);

        $this->assertDatabaseHas('articles', [
            'feed_id' => '102caa00-fe4c-5f6a-bdc0-8650e3ba1960',
        ]);

        $this->assertDatabaseHas('articles', [
            'feed_id' => 'cc0d2ee3-5dea-55f6-9cd5-ca04c5180590',
        ]);
    }

    public function testFailIfWrongFileProvided()
    {
        $response = $this->postJson(route('articles.import-feed'), [
            'file' => $this->uploadTestFile(
                base_path('tests/Services/FeedLoader/test_feed_duplicate.json'),
            ),
        ]);

        $response->assertStatus(400);
        $response->assertJson(['imported' => false]);

        $this->assertDatabaseMissing('articles', [
            'feed_id' => 'cc0d2ee3-5dea-55f6-9cd5-ca04c5180590',
        ]);
    }

    public function testGetAllCategories()
    {
        $response = $this->postJson(route('articles.import-feed'), [
            'file' => $this->uploadTestFile(
                base_path('tests/Services/FeedLoader/test_feed.json'),
            ),
        ]);

        $response->assertSuccessful();

        $response = $this->getJson(route('articles.categories'));

        $response->assertSuccessful();
        $response->assertJsonCount(Category::count('id'));
        $response->assertJsonStructure([['name', 'id']]);
    }

    public function testSearchArticles()
    {
        $this->seed(ArticleSeeder::class);

        $response = $this->getJson(route('articles.list', ['text' => 'Fujifilm Instax Mini']));

        $response->assertSuccessful();
        $response->assertJsonCount(1);

        $response = $this->getJson(
            route('articles.list', [
                'category' => Category::firstWhere(['name' => 'tech'])->id,
            ]),
        );
        $response->assertJsonCount(2);
    }

    public function testGetArticleBySlug()
    {
        $this->seed(ArticleSeeder::class);

        $response = $this->getJson(
            route('articles.find-by-slug', [
                'slug' => 'cheap-say-cheese-fool-the-fujifilm-instax-mini-9-bundle-has-40-off',
            ]),
        );

        $response->assertSuccessful();

        $this->assertStringContainsString('Fujifilm Instax Mini 9 bundle has 40% off', $response->json('title'));
    }
}
