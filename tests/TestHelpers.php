<?php

namespace Tests;

use Illuminate\Http\UploadedFile;

/**
 * @mixin \Illuminate\Foundation\Testing\TestCase
 */
trait TestHelpers
{
    public function assertContainsAll(array $expected, array $targetArray, $message = '')
    {
        foreach ($expected as $item) {
            $this->assertContains($item, $targetArray, $message);
        }
    }

    function uploadTestFile(string $path): UploadedFile
    {
        return new UploadedFile(
            $path,
            pathinfo($path, PATHINFO_BASENAME),
            \File::mimeType($path),
            UPLOAD_ERR_OK,
            true,
        );
    }
}
