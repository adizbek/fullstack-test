<?php

namespace Tests\Repositories;

use App\Models\Category;
use App\Repositories\CategoryRepository;
use Tests\TestCase;

class CategoryRepositoryTest extends TestCase
{
    public function testCreateCategories()
    {
        $repository = app()->make(CategoryRepository::class);

        $repository->createCategories(['test_cat1', 'test_cat2', 'test_cat3']);

        $this->assertDatabaseHas('categories', ['name' => 'test_cat1']);
        $this->assertDatabaseHas('categories', ['name' => 'test_cat2']);
        $this->assertDatabaseHas('categories', ['name' => 'test_cat3']);
    }

    public function testGetCategoriesByName()
    {
        $repository = app()->make(CategoryRepository::class);

        $names = ['test_cat1', 'test_cat2', 'test_cat3'];

        $repository->createCategories($names);

        $categories = $repository->getCategoriesByName($names);

        $this->assertCount(3, $categories);

        foreach ($categories as $category) {
            $this->assertContains($category->name, $names);
        }
    }

    public function testGetAllCategories() {
        $repository = app()->make(CategoryRepository::class);

        $names = ['test_cat1', 'test_cat2', 'test_cat3'];

        $repository->createCategories($names);

        $categories = $repository->getAllCategories();

        $this->assertCount(Category::count('id'), $categories);
    }
}
