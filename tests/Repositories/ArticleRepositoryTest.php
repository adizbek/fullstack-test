<?php

namespace Tests\Repositories;

use App\Domain\Article\DTO\ListArticlesDTO;
use App\Domain\Feed\FeedLoader\JSONFileLoader;
use App\Domain\Feed\FeedService;
use App\Models\Article;
use App\Models\Category;
use App\Repositories\ArticleRepository;
use Database\Seeders\ArticleSeeder;
use Illuminate\Database\QueryException;
use Tests\TestCase;

class ArticleRepositoryTest extends TestCase
{
    public function testCreateArticlesFromFeeds()
    {
        $this->seed(ArticleSeeder::class);

        $this->assertDatabaseHas('articles', [
            'feed_id' => '102caa00-fe4c-5f6a-bdc0-8650e3ba1960',
        ]);
        $this->assertDatabaseHas('articles', [
            'feed_id' => 'cc0d2ee3-5dea-55f6-9cd5-ca04c5180590',
        ]);

        $article = Article::whereFeedId('102caa00-fe4c-5f6a-bdc0-8650e3ba1960')
            ->with('categories')
            ->first();

        $this->assertContainsAll(
            ['syndication', 'tech', 'space'],
            $article->categories->pluck('name')->toArray(),
        );
        $this->assertCount(3, $article->categories);
        $this->assertEquals(
            'syndication',
            $article->categories->firstWhere('pivot.is_primary', '=', true)->name,
        );

        $article = Article::whereFeedId('cc0d2ee3-5dea-55f6-9cd5-ca04c5180590')
            ->with('categories')
            ->first();

        $this->assertContainsAll(
            ['plugged', 'tech'],
            $article->categories->pluck('name')->toArray(),
        );
        $this->assertEquals(
            'plugged',
            $article->categories->firstWhere('pivot.is_primary', '=', true)->name,
        );
        $this->assertCount(2, $article->categories);
    }

    public function testShouldNotThrowIfProvidedEmptyFeed()
    {
        $this->expectNotToPerformAssertions();

        $repository = app()->make(FeedService::class);
        $repository->saveFeeds([]);
    }

    public function testShouldFailIfTwoSimilarArticlesProvided()
    {
        $this->expectException(QueryException::class);

        $loader = new JSONFileLoader(base_path('tests/Services/FeedLoader/test_feed.json'));
        $articles = $loader->load();

        $repository = app()->make(FeedService::class);
        $repository->saveFeeds([clone $articles[0], clone $articles[0]]);
    }

    public function testShouldFindAllArticles()
    {
        $this->seed(ArticleSeeder::class);

        $repository = app()->make(ArticleRepository::class);
        $articles = $repository->getArticles(new ListArticlesDTO('Fujifilm Instax Mini'));

        $this->assertGreaterThanOrEqual(1, count($articles));

        $category = Category::whereName('tech')->first();
        $articles = $repository->getArticles(new ListArticlesDTO(categoryId: $category->id));
        $this->assertGreaterThanOrEqual(2, count($articles));

        foreach ($articles as $article) {
            $article = Article::with('media')->find($article->id);
            $this->assertGreaterThanOrEqual(1, count($article->media));
        }
    }

    public function testGetArticleBySlug()
    {
        $this->seed(ArticleSeeder::class);

        $repository = app()->make(ArticleRepository::class);
        $article = $repository->getArticleBySlug(
            'cheap-say-cheese-fool-the-fujifilm-instax-mini-9-bundle-has-40-off',
        );

        $this->assertStringContainsString('CHEAP', $article->title);
    }
}
