import Vue from 'vue'
import VueRouter from 'vue-router'
import ImportFeedPage from '../pages/ImportFeedPage'
import HomePage from '../pages/HomePage'
import ArticlePage from '../pages/ArticlePage'

Vue.use(VueRouter)

const routes = [
    { path: '/importFeed', component: ImportFeedPage, name: 'import-feed' },
    { path: '/', component: HomePage },
    { path: '/article/:slug', component: ArticlePage, name: 'article-show' },
]

const router = new VueRouter({
    routes,
    mode: 'hash',
})

export default router
