import Vue from 'vue'

const axios = require('axios')

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
axios.defaults.headers.common['Accept'] = 'application/json'

Vue.prototype.$http = axios.create({
    baseURL: '/api',
})
