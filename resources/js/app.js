import App from './components/App'
import Vue from 'vue'

require('./bootstrap')
import plugins from './plugins'

window.Vue = Vue

// register all components recursively
const files = require.context('./', true, /\.vue$/i)
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

const app = new Vue({
    ...plugins,
    el: '#app',
    render: h => h(App),
})
