interface Article {
    title: string
    content: string

    media: Media[]
    categories: Category[]
}

interface Category {
    id: number
    name: string
}

interface Media {
    id: number
    source: string
    slug: string
    type: string | 'featured'
    attributes: { width: number; height: number; url: string }
}
