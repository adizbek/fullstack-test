FROM phpswoole/swoole:4.8.6-php8.1-alpine

# Working directory was set to /var/www

ENV APP_ENV production

RUN apk add --no-cache zip unzip git libxml2-dev oniguruma-dev libpng-dev icu-dev autoconf g++ make nodejs npm postgresql-dev && \
    pecl install redis && docker-php-ext-enable redis

# Install PHP extensions
RUN docker-php-ext-install pdo_mysql pdo_pgsql mbstring exif pcntl bcmath gd opcache intl

COPY composer.* ./

RUN composer install --no-dev --no-cache --no-scripts

COPY package-lock.json package.json ./

RUN npm ci

COPY . .

# install roadrunner
RUN composer du && npm run prod

CMD php artisan octane:start --host=0.0.0.0 --server=swoole
