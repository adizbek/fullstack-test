# Full-Stack Developer Test

## Brief

Develop a combined backend and frontend application that consumes a 200-article JSON feed, implements simple database search querying, and displays a (at minimum, barebones) website with basic category and search filtering.

## Requirements

We expect to see:

-   [x] A Laravel backend application in PHP that consumes the JSON feed, publishes it to a database, and exposes the database via APIs.
-   [x] These APIs should expose retrieval of lists of articles, and can perform simple filtering (against categories) and querying (search) of the article main body and title
    -   The quality of the search results is not judged
-   [x] A database that stores the JSON feed in a well-formed way, with appropriate indexes
-   [x] An Vue.JS SPA frontend that calls the backend API, shows the articles and has UX for filtering the category names and a search box
-   [x] The design of the frontend is not important and is not judged in this test
-   [x] Memory usage should be a consideration
-   [x] A full git log showing work done
-   [x] A README to explain how to boot and run the end result

What would be nice to have:

-   [x] Unit tests, All functionality has been tested using tests
-   [x] Migrations, to create database used laravel migrations
-   [x] Full docblock comments, I wrote code using SOLID principles, tried to make code clean as much as I could, I used php 8.1, in this version has very good support of types, so we don't need to describe docblocks.
-   [x] PHP 7.4 usage, I've used php 8.1
                                              
# How to run
- `docker-compose up -d` starts docker containers, you should wait few moments until db gets ready
- `docker-compose exec app php artisan migrate` migrates databases

Now you should be able to access site at `http://localhost:8000`

- Visit site, import json feed, there is link which navigates to import page, import data
- Visit home page, now you should see the articles, you can search by title, content and category

# Technical questions

Please answer the following questions in a markdown file called <code>Answers to technical questions.md</code>

-   How long did you spend on the coding test? What would you add to your solution if you had more time? If you didn't spend much time on the coding test then use this as an opportunity to explain what you would add.
-   How would you track down a performance issue in production? Have you ever had to do this?
-   Please describe yourself using JSON.

1. I've spent around 8 active hours. I would like to improve feed importing feature, currently if you import duplicate feeds, it fails to import even there are some new ones
2. I've used newrelic and Elastic APM, especially newrelic provides good tools to track performance of your application, as well as tracking many others metrics
3. Okay

```json
{
    "name": "Adizbek",
    "surname": "Ergashev",
    "age": 23,
    "years_in_coding": 8,
    "years_in_commercial_coding": 6,
    "skills": [
        "laravel",
        "vuejs",
        "nodejs",
        "android",
        "mysql",
        "postgresql",
        "kotlin",
        "java",
        "redis",
        "rabbitmq",
        "docker",
        "devops"
    ],
    "hobbies": ["sports", "reading books", "self-study", "coding in free time"],
    "contacts": [
        {
            "url": "https://www.linkedin.com/in/adizbek",
            "type": "linkedin"
        },
        {
            "url": "https://t.me/adizbek",
            "type": "telegram"
        }
    ],
    "work_experience": [
        {
            "company": "Realsoft",
            "position": "Senior Vuejs developer",
            "start": "2019-06-01",
            "end": null
        },
        {
            "company": "UmbrellaSoft",
            "position": "Android developer",
            "start": "2017-09-01",
            "end": "2019-06-01"
        },
        {
            "company": "Freelance",
            "position": "Freelancer",
            "start": "2016-02-01",
            "end": "2017-09-01"
        }
    ],

    "education": [
        {
            "institution": "Tashkent information technologies university",
            "degree": "bachelors",
            "start": "2017-09-01",
            "end": "2021-06-01"
        },
        {
            "institution": "Tashkent information technologies university",
            "degree": "masters",
            "start": "2021-09-01",
            "end": null
        }
    ]
}
```
