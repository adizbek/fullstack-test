<?php

namespace App\Repositories;

use App\Domain\Article\DTO\ArticleDTO;
use App\Domain\Article\DTO\ListArticlesDTO;
use App\Domain\Feed\DTO\FeedArticle;
use App\Models\Article;
use Illuminate\Database\Eloquent\Builder;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

class ArticleRepository
{
    /**
     * @param FeedArticle[] $feeds
     */
    public function createArticlesFromFeeds(array $feeds)
    {
        $feedsToInsert = [];

        foreach ($feeds as $feed) {
            $feedsToInsert[] = [
                'feed_id' => $feed->getFeedId(),
                'source' => $feed->getSource(),
                'title' => $feed->getTitle(),
                'subtitle' => $feed->getSubtitle(),
                'format' => $feed->getFormat(),
                'emoji' => $feed->getEmoji(),
                'slug' => $feed->getSlug(),
                'content' => $feed->getContent(),
                'created_at' => now(),
                'updated_at' => now(),
            ];
        }

        Article::insert($feedsToInsert);
    }

    /**
     * @param ListArticlesDTO $filters
     * @return ArticleDTO[]
     */
    public function getArticles(ListArticlesDTO $filters): array
    {
        $articles = Article::when($filters->text, function (Builder $builder, string $text) {
            $builder->where(
                fn(Builder $builder) => $builder
                    ->whereRaw('title ilike ?', ["%$text%"])
                    ->orWhereRaw('content ilike ?', ["%$text%"]),
            );
        })
            ->when($filters->categoryId, function (Builder $builder, int $category) {
                $builder->whereHas('categories', function (Builder $builder) use ($category) {
                    $builder->where('id', $category);
                });
            })
            ->limit(20)
            ->with('media', 'categories')
            ->get(['id', 'title', 'subtitle', 'content', 'slug'])
            ->toArray();

        return ArticleDTO::arrayOf($articles);
    }

    public function getArticleBySlug(string $slug): ?ArticleDTO
    {
        try {
            return new ArticleDTO(
                Article::whereSlug($slug)
                    ->with('media', 'categories')
                    ->first(['id', 'title', 'subtitle', 'content', 'slug'])
                    ->toArray(),
            );
        } catch (UnknownProperties $e) {
            return null;
        }
    }
}
