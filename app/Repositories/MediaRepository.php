<?php

namespace App\Repositories;

use App\Domain\Feed\DTO\FeedArticle;
use App\Domain\Feed\DTO\FeedMedia;
use App\Domain\Media\FeedMediaLoader;
use App\Models\Media;

class MediaRepository
{
    /**
     * @param FeedArticle[] $feeds
     * @return FeedMedia[]
     */
    public function createMediasFromFeeds(array $feeds): array
    {
        $medias = (new FeedMediaLoader($feeds))->load();

        $this->createFeedMedias($medias);

        return $medias;
    }

    /**
     * @param FeedMedia[] $medias
     * @return void
     */
    private function createFeedMedias(array $medias): void
    {
        $data = array_map(function (FeedMedia $media) {
            return [
                'media_id' => $media->getMediaId(),
                'source' => $media->getSource(),
                'slug' => $media->getSlug(),
                'type' => $media->getType(),
                'attributes' => json_encode($media->getAttributes()),
            ];
        }, $medias);

        Media::insert($data);
    }

    /**
     * @param array $slugs
     * @return array
     */
    public function getMediasIdBySlug(array $slugs): array
    {
        return Media::whereIn('slug', $slugs)->toBase()->get(['id', 'slug'])->toArray();
    }
}
