<?php

namespace App\Repositories;

use App\Domain\Category\DTO\CategoryDTO;
use App\Domain\Category\FeedCategoryLoader;
use App\Domain\Feed\DTO\FeedArticle;
use App\Models\Category;

class CategoryRepository
{
    /**
     * @param array $categories
     * @return bool
     */
    public function createCategories(array $categories): bool
    {
        $data = array_map(function ($name) {
            return ['name' => $name];
        }, $categories);

        return Category::insert($data);
    }

    /**
     * @param FeedArticle[] $feeds
     * @return string[]
     */
    public function createCategoriesFromFeeds(array $feeds): array
    {
        $categoriesNames = (new FeedCategoryLoader($feeds))->load();

        $this->createCategories($categoriesNames);

        return $categoriesNames;
    }

    /**
     * @return CategoryDTO[]
     */
    public function getCategoriesByName($names): array
    {
        return CategoryDTO::arrayOf(
            Category::whereIn('name', $names)
                ->get(['id', 'name'])
                ->toArray(),
        );
    }

    /**
     * @return CategoryDTO[]
     */
    public function getAllCategories(): array
    {
        return CategoryDTO::arrayOf(
            Category::all(['id', 'name'])
                ->toArray(),
        );
    }
}
