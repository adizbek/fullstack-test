<?php

namespace App\Service;

use App\Domain\Feed\FeedLoader\JSONFileLoader;
use App\Domain\Feed\FeedService;
use Illuminate\Database\QueryException;
use SplFileInfo;

class ArticleService
{
    private FeedService $feedService;

    public function __construct(FeedService $feedService)
    {
        $this->feedService = $feedService;
    }

    /**
     * @throws QueryException
     */
    public function importFeed(SplFileInfo $file)
    {
        $feedLoader = new JSONFileLoader($file->getRealPath());

        $this->feedService->saveFeeds($feedLoader->load());
    }
}
