<?php

namespace App\Domain\Feed\Request;

use Illuminate\Foundation\Http\FormRequest;

class ImportFeedRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'file' => 'required|file',
        ];
    }
}
