<?php

namespace App\Domain\Feed\DTO;

class FeedArticle
{
    protected string $title;
    protected string $subtitle;
    protected string $source;
    protected string $format;
    protected string $slug;
    protected ?string $content;
    protected string $feedId;
    protected string $emoji;

    /**
     * @var string[]
     */
    protected array $categories;

    /**
     * @var FeedMedia[]
     */
    protected array $media = [];

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return FeedArticle
     */
    public function setTitle(string $title): FeedArticle
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubtitle(): string
    {
        return $this->subtitle;
    }

    /**
     * @param string $subtitle
     * @return FeedArticle
     */
    public function setSubtitle(string $subtitle): FeedArticle
    {
        $this->subtitle = $subtitle;
        return $this;
    }

    /**
     * @return string
     */
    public function getSource(): string
    {
        return $this->source;
    }

    /**
     * @param string $source
     * @return FeedArticle
     */
    public function setSource(string $source): FeedArticle
    {
        $this->source = $source;
        return $this;
    }

    /**
     * @return string
     */
    public function getFormat(): string
    {
        return $this->format;
    }

    /**
     * @param string $format
     * @return FeedArticle
     */
    public function setFormat(string $format): FeedArticle
    {
        $this->format = $format;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     * @return FeedArticle
     */
    public function setSlug(string $slug): FeedArticle
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return ?string
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string|null $content
     * @return FeedArticle
     */
    public function setContent(?string $content): FeedArticle
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return string
     */
    public function getFeedId(): string
    {
        return $this->feedId;
    }

    /**
     * @param string $feedId
     * @return FeedArticle
     */
    public function setFeedId(string $feedId): FeedArticle
    {
        $this->feedId = $feedId;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmoji(): string
    {
        return $this->emoji;
    }

    /**
     * @param string $emoji
     * @return FeedArticle
     */
    public function setEmoji(string $emoji): FeedArticle
    {
        $this->emoji = $emoji;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getCategories(): array
    {
        return $this->categories;
    }

    /**
     * @param string[] $categories
     * @return FeedArticle
     */
    public function setCategories(array $categories): FeedArticle
    {
        $this->categories = $categories;
        return $this;
    }

    /**
     * @return FeedMedia[]
     */
    public function getMedia(): array
    {
        return $this->media;
    }

    /**
     * @param FeedMedia[] $media
     * @return FeedArticle
     */
    public function setMedia(array $media): FeedArticle
    {
        $this->media = $media;
        return $this;
    }
}
