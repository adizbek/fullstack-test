<?php

namespace App\Domain\Feed\DTO;

class FeedMedia
{
    protected string $mediaId;
    protected string $source;
    protected string $slug;
    protected string $type;
    protected \stdClass $attributes;

    /**
     * @return string
     */
    public function getMediaId(): string
    {
        return $this->mediaId;
    }

    /**
     * @param string $mediaId
     * @return FeedMedia
     */
    public function setMediaId(string $mediaId): FeedMedia
    {
        $this->mediaId = $mediaId;
        return $this;
    }

    /**
     * @return string
     */
    public function getSource(): string
    {
        return $this->source;
    }

    /**
     * @param string $source
     * @return FeedMedia
     */
    public function setSource(string $source): FeedMedia
    {
        $this->source = $source;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     * @return FeedMedia
     */
    public function setSlug(string $slug): FeedMedia
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return FeedMedia
     */
    public function setType(string $type): FeedMedia
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return \stdClass
     */
    public function getAttributes(): \stdClass
    {
        return $this->attributes;
    }

    /**
     * @param \stdClass $attributes
     * @return FeedMedia
     */
    public function setAttributes(\stdClass $attributes): FeedMedia
    {
        $this->attributes = $attributes;
        return $this;
    }
}
