<?php

namespace App\Domain\Feed;

use App\Domain\Category\DTO\CategoryDTO;
use App\Domain\Feed\DTO\FeedArticle;
use App\Domain\Feed\DTO\FeedMedia;
use App\Domain\Feed\FeedLoader\IFeedSaver;
use App\Models\Article;
use App\Repositories\ArticleRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\MediaRepository;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use stdClass;

class FeedService implements IFeedSaver
{
    private ArticleRepository $articleRepository;
    private CategoryRepository $categoryRepository;
    private MediaRepository $mediaRepository;

    public function __construct(
        ArticleRepository $articleRepository,
        CategoryRepository $categoryRepository,
        MediaRepository $mediaRepository,
    ) {
        $this->articleRepository = $articleRepository;
        $this->categoryRepository = $categoryRepository;
        $this->mediaRepository = $mediaRepository;
    }

    /**
     * @throws QueryException
     *
     * @param array $feeds
     * @return void
     */
    public function saveFeeds(array $feeds)
    {
        if (count($feeds) == 0) {
            return;
        }

        DB::transaction(function () use ($feeds) {
            $this->articleRepository->createArticlesFromFeeds($feeds);
            $insertedCategoryNames = $this->categoryRepository->createCategoriesFromFeeds(
                $feeds,
            );
            $insertedMedias = $this->mediaRepository->createMediasFromFeeds($feeds);

            $lastId = Article::latest('id')->first(['id'])->id;
            $this->assignCategoriesToArticles($feeds, $lastId, $insertedCategoryNames);
            $this->assignMediasToArticles($feeds, $lastId, $insertedMedias);
        });
    }

    private function assignCategoriesToArticles(
        array $feeds,
        int $lastArticleId,
        array $insertedCategoryNames,
    ) {
        $categories = $this->categoryRepository->getCategoriesByName($insertedCategoryNames);

        $categoriesKeyByName = collect($categories)->reduce(function (
            array $categories,
            CategoryDTO $category,
        ) {
            $categories[$category->name] = $category->id;

            return $categories;
        },
        []);

        $relatedCategories = [];

        for ($i = count($feeds) - 1, $seq = 0; $i >= 0; $i--, $seq++) {
            $articleId = $lastArticleId - $seq;

            $feedCategories = $feeds[$i]->getCategories();

            for ($j = 0; $j < count($feedCategories); $j++) {
                $relatedCategories[] = [
                    'category_id' => $categoriesKeyByName[$feedCategories[$j]],
                    'article_id' => $articleId,
                    'is_primary' => $j == 0,
                ];
            }
        }

        DB::table('article_categories')->insert($relatedCategories);
    }

    /**
     * @param FeedArticle[] $feeds
     * @param int $lastArticleId
     * @param FeedMedia[] $insertedMedias
     * @return void
     */
    private function assignMediasToArticles(
        array $feeds,
        int $lastArticleId,
        array $insertedMedias,
    ) {
        $slugs = collect($insertedMedias)
            ->map(fn(FeedMedia $item) => $item->getSlug())
            ->toArray();

        $medias = collect($this->mediaRepository->getMediasIdBySlug($slugs))->reduce(function (
            array $medias,
            stdClass $media,
        ) {
            $medias[$media->slug] = $media->id;

            return $medias;
        },
        []);

        $relatedMedia = [];

        for ($i = count($feeds) - 1, $seq = 0; $i >= 0; $i--, $seq++) {
            $articleId = $lastArticleId - $seq;

            foreach ($feeds[$i]->getMedia() as $media) {
                $relatedMedia[] = [
                    'media_id' => $medias[$media->getSlug()],
                    'article_id' => $articleId,
                ];
            }
        }

        DB::table('article_media')->insert($relatedMedia);
    }
}
