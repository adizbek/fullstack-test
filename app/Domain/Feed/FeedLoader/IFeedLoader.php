<?php

namespace App\Domain\Feed\FeedLoader;

use App\Domain\Feed\DTO\FeedArticle;

interface IFeedLoader
{
    /**
     * @return FeedArticle[]
     */
    public function load(): array;
}
