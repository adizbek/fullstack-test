<?php

namespace App\Domain\Feed\FeedLoader;

abstract class AbstractFileFeedLoader implements IFeedLoader
{
    protected string $filePath;

    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;
    }
}
