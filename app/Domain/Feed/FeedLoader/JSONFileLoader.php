<?php

namespace App\Domain\Feed\FeedLoader;

use App\Domain\Feed\DTO\FeedArticle;
use App\Domain\Feed\DTO\FeedMedia;
use stdClass;

class JSONFileLoader extends AbstractFileFeedLoader
{
    /**
     * @return FeedArticle[]
     */
    public function load(): array
    {
        $feed = json_decode(file_get_contents($this->filePath));

        return $this->extractArticles($feed);
    }

    /**
     * @param array $feed
     * @return FeedArticle[]
     */
    protected function extractArticles(array $feed): array
    {
        $articles = [];

        foreach ($feed as $article) {
            $item = new FeedArticle();

            if (isset($article->media) && is_array($article->media)) {
                $item->setMedia($this->extractMedia($article->media));
            }

            $item
                ->setSlug($article->slug)
                ->setFeedId($article->id)
                ->setTitle($article->title)
                ->setSubtitle($article->subtitle)
                ->setSource($article->source)
                ->setFormat($article->format)
                ->setEmoji($article->emoji)
                ->setContent($this->extractContent($article))
                ->setCategories($this->extractCategories($article));

            $articles[] = $item;
        }

        return $articles;
    }

    /**
     * @param stdClass[] $medias
     * @return FeedMedia[]
     */
    protected function extractMedia(array $medias): array
    {
        $items = [];

        foreach ($medias as $media) {
            $item = new FeedMedia();

            $item
                ->setMediaId($media->media->id)
                ->setSlug($media->media->slug)
                ->setSource($media->media->source)
                ->setType($media->type)
                ->setAttributes($media->media->attributes);

            $items[] = $item;
        }

        return $items;
    }

    protected function extractContent(stdClass $article): ?string
    {
        if (
            isset($article->content) &&
            is_array($article->content) &&
            isset($article->content[0]->content)
        ) {
            return $article->content[0]->content;
        }

        return null;
    }

    /**
     * @param stdClass $article
     * @return string[]
     */
    protected function extractCategories(stdClass $article): array
    {
        $categories = [];

        if (isset($article->categories)) {
            if (
                isset($article->categories->primary) &&
                is_string($article->categories->primary)
            ) {
                $categories[] = $article->categories->primary;
            }

            if (
                isset($article->categories->additional) &&
                is_array($article->categories->additional)
            ) {
                foreach ($article->categories->additional as $category) {
                    $categories[] = $category;
                }
            }
        }

        return $categories;
    }
}
