<?php

namespace App\Domain\Feed\FeedLoader;

use App\Domain\Feed\DTO\FeedArticle;

interface IFeedSaver
{
    /**
     * @param FeedArticle[] $feeds
     */
    public function saveFeeds(array $feeds);
}
