<?php

namespace App\Domain\Category\DTO;

use Spatie\DataTransferObject\DataTransferObject;

class CategoryDTO extends DataTransferObject
{
    public string $name;
    public int $id;

}
