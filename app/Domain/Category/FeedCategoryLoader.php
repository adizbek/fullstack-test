<?php

namespace App\Domain\Category;

use App\Domain\Feed\DTO\FeedArticle;
use JetBrains\PhpStorm\Pure;

class FeedCategoryLoader
{
    /**
     * @var FeedArticle[]
     */
    private array $articles;

    /**
     * @param FeedArticle[] $articles
     */
    public function __construct(array $articles)
    {
        $this->articles = $articles;
    }

    /**
     * @return string[]
     */
    #[Pure]
    public function load(): array
    {
        $categories = [];

        foreach ($this->articles as $article) {
            foreach ($article->getCategories() as $category) {
                $categories[$category] = 1;
            }
        }

        return array_keys($categories);
    }
}
