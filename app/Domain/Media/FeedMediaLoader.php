<?php

namespace App\Domain\Media;

use App\Domain\Feed\DTO\FeedArticle;
use App\Domain\Feed\DTO\FeedMedia;
use JetBrains\PhpStorm\Pure;

class FeedMediaLoader
{
    /**
     * @var FeedArticle[]
     */
    private array $articles;

    /**
     * @param FeedArticle[] $articles
     */
    public function __construct(array $articles)
    {
        $this->articles = $articles;
    }

    /**
     * @return FeedMedia[]
     */
    #[Pure]
    public function load(): array
    {
        $medias = [];

        foreach ($this->articles as $article) {
            foreach ($article->getMedia() as $item) {
                $medias[$item->getSlug()] = $item;
            }
        }

        return array_values($medias);
    }
}
