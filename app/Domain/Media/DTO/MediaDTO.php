<?php

namespace App\Domain\Media\DTO;

use Spatie\DataTransferObject\DataTransferObject;

class MediaDTO extends DataTransferObject
{
    public int $id;

    public string $source;

    public string $slug;

    public string $type;

    public \stdClass $attributes;

}
