<?php

namespace App\Domain\Article\DTO;

use App\Domain\Category\DTO\CategoryDTO;
use App\Domain\Media\DTO\MediaDTO;
use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Spatie\DataTransferObject\DataTransferObject;

class ArticleDTO extends DataTransferObject
{
    public int $id;

    public ?string $source;

    public ?string $title;

    public ?string $subtitle;

    public ?string $format;

    public ?string $emoji;

    public ?string $content;

    public ?string $slug;

    #[CastWith(ArrayCaster::class, itemType: MediaDTO::class)]
    public ?array $media;

    #[CastWith(ArrayCaster::class, itemType: CategoryDTO::class)]
    public ?array $categories;
}
