<?php

namespace App\Domain\Article\DTO;

class ListArticlesDTO
{
    public ?int $categoryId;
    public ?string $text;

    /**
     * @param int|null $categoryId
     * @param string|null $text
     */
    public function __construct(?string $text = null, ?int $categoryId = null)
    {
        $this->categoryId = $categoryId;
        $this->text = $text;
    }
}
