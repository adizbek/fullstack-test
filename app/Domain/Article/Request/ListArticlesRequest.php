<?php

namespace App\Domain\Article\Request;

use App\Domain\Article\DTO\ListArticlesDTO;
use Illuminate\Foundation\Http\FormRequest;

class ListArticlesRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'text' => 'nullable|string',
            'category' => 'nullable|numeric|exists:categories,id'
        ];
    }

    public function validated(): ListArticlesDTO
    {
        $data = parent::validated();

        return new ListArticlesDTO($data['text'] ?? null, $data['category'] ?? null);
    }


}
