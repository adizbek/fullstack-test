<?php

namespace App\Http\Controllers\Api;

use App\Domain\Article\Request\ListArticlesRequest;
use App\Domain\Feed\Request\ImportFeedRequest;
use App\Http\Controllers\Controller;
use App\Repositories\ArticleRepository;
use App\Repositories\CategoryRepository;
use App\Service\ArticleService;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;

class ArticleController extends Controller
{
    public function importFeed(
        ImportFeedRequest $request,
        ArticleService $service,
    ): JsonResponse {
        try {
            $service->importFeed($request->validated()['file']);

            return response()->json(['imported' => true]);
        } catch (QueryException $exception) {
            return response()->json(
                [
                    'imported' => false,
                    'message' => $exception->getMessage(),
                ],
                400,
            );
        }
    }

    public function findBySlug(string $slug, ArticleRepository $repository): JsonResponse
    {
        return response()->json($repository->getArticleBySlug($slug));
    }

    public function listArticles(
        ArticleRepository $repository,
        ListArticlesRequest $request,
    ): JsonResponse {
        return response()->json($repository->getArticles($request->validated()));
    }

    public function getCategories(CategoryRepository $repository): JsonResponse
    {
        return response()->json($repository->getAllCategories());
    }
}
